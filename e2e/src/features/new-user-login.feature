Feature: As a user I expect to be able to login

    @dev
    Scenario: As a new user I expect to be able to create a password
        Given I am on the "login" page
        And I click the "create password" button
        Then the "cognizant email input" element should be displayed
