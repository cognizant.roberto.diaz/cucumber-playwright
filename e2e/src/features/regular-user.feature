Feature: As a regular user I should be able to interact with the application

    @dev
    Scenario: As a new regular user I expect to be able to create a password
        Given I am on the "login" page
        And I click the "create password" button
        Then the "cognizant email input" should be displayed

    @regression
    Scenario: TC1: As a new regular user I expect to be able to create my profile
        Given I am on the "login" page
        And I fill in the "email input" with "jose.diazalvarez@cognizant.com"
        And I type in the "password input" with "******"
        And I click the "sign in button" element
        Then I am directed to the "wizard" page
    #Personal Information
        And I fill in the "first name input" with "Jose Roberto"
        And I fill in the "last name input" with "Diaz Alvarez"
        And I select the "QA Engineer" option from the "position dropdown"
        And I select the "Remote position" option from the "studio dropdown"
        And I select the "Quality Engineering" option from the "community dropdown"
        And I fill in the "email input" with "jose.diazalvarez@cognizant.com"
        And I click the "next" button
    #Summary
        Then the "about me textarea" should be displayed
        And I fill in the "about me textarea" with "QA Engineer with X years of experience"
        And I click the "next" button
    #Experience highlights
        Then the "add experience highlights button" should be displayed
        And I click the "add experience highlights button" button
        And I fill in the "highlight description input" with "QA Automation experience"
        #Add Save button click here
        And I click the "next" button
    #Skills
        Then the "add skill button" should be displayed
        And I click the "add skill button" button
        And I fill in the "skill name input" with "TypeScript"
        #Add level interaction here
        #Add years of Experience here
        #Add Save button click here
        And I click the "next" button
    #Education
        Then the "add education button" should be displayed
        And I click the "add education button" button
        And I fill in the "career input" with "Computer Systems Engineer"
        And I fill in the "institution input" with "Instituto Tecnologico de Zacatepec"
        And I fill in the "start input" with "2010"
        And I fill in the "end input" with "2015"
        #Add Save button click here
        And I click the "add certification button" button
        And I fill in the "certification input" with "Cucumber playwright typescript framework"
        And I fill in the "institution input" with "Udemy"
        And I fill in the "start input" with "2023"
        And I fill in the "end input" with "2023"
        #Add Save button click here
        And I click the "next" button
    #Work Experience
        Then the "add work experience button" should be displayed
        And I click the "add work experience button" button
        And I fill in the "company input" with "HCL Technologies"
        And I fill in the "role input" with "Software Developer"
        And I fill in the "location input" with "Guadalajara"
        And I fill in the "start input" with "2015"
        And I fill in the "end input" with "2018"
        And I fill in the "achievement textarea" with "Learnt and matured programming skills"
        #Add Save button click here
        And I click the "done" button
    #Success
        Then the "Success OK button" should be displayed
        And I click the "Success OK button" button
        Then I am directed to the "profile" page