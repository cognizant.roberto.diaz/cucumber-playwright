import { Page } from "playwright";
import { ElementLocator } from "../env/global";

export const clickElement = async(
    page: Page,
    elementIdentifier: ElementLocator,
): Promise<void> => {
    await page.click(elementIdentifier);
}

export const fillInValue = async (
    page: Page,
    elementIdentifier: ElementLocator,
    input: string,
): Promise<void> => {
    await page.focus(elementIdentifier)
    await page.fill(elementIdentifier, input)
}

export const typeInValue = async (
    page: Page,
    elementIdentifier: ElementLocator,
    input: string,
): Promise<void> => {
    await page.focus(elementIdentifier)
    await page.type(elementIdentifier, input)
}

export const selectValue = async (
    page: Page,
    elementIdentifier: ElementLocator,
    option: string,
): Promise<void> => {
    await page.click(elementIdentifier);
    //scroll to option
    /*
        await page.getByLabel('Position', { exact: true }).click();
        await page.getByText('R&D Manager').click();
        await page.locator('[id="Personal\\ Information"]').getByTitle('R&D Manager').click();
        await page.getByText('QA Engineer').click();
        await page.getByLabel('Studio').click();
    */
   
    /*
        const article = page.locator( `div.maybe-scrollable`);
        const isScrollable = await article.evaluate(e => e.clientHeight < e.scrollHeight);
        // scroll to top
        await article.evaluate(e => e.scrollTop = 0);
    */
    let loc = page.locator(`text="${option}"`);
    await loc.scrollIntoViewIfNeeded();
    await page.getByText(option).click({force: true});
}