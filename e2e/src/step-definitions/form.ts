import { Then } from '@cucumber/cucumber';
import { waitFor, waitForSelector } from '../support/wait-for-behavior';
import { getElementLocator } from '../support/web-element-helper';
import { ScenarioWorld } from './setup/world'; 
import { ElementKey } from '../env/global';
import { fillInValue, typeInValue, selectValue } from '../support/html-behavior';

Then (
    /^I fill in the "([^"]*)" with "([^"]*)"$/,
    async function(this: ScenarioWorld, elementKey: ElementKey, input: string) {
        const {
            screen: { page },
            globalConfig,
        } = this;
        
        console.log(`I fill in the ${elementKey} with ${input}`);
        
        const elementIdentifier = getElementLocator(page, elementKey, globalConfig);

        await waitFor( async () => {
            //const elementStable = await waitForSelector(page, elementIdentifier);
            const result = await page.waitForSelector(elementIdentifier, {
                state: 'visible',
            });
            if(result) {
                await fillInValue(page, elementIdentifier, input)
            }
            return result;
        });
    }
);

Then (
    /^I type in the "([^"]*)" with "([^"]*)"$/,
    async function(this: ScenarioWorld, elementKey: ElementKey, input: string) {
        const {
            screen: { page },
            globalConfig,
        } = this;
        
        console.log(`I fill in the ${elementKey} with ${input}`);
        
        const elementIdentifier = getElementLocator(page, elementKey, globalConfig);

        await waitFor( async () => {
            //const elementStable = await waitForSelector(page, elementIdentifier);
            const result = await page.waitForSelector(elementIdentifier, {
                state: 'visible',
            });
            if(result) {
                await typeInValue(page, elementIdentifier, input)
            }
            return result;
        });
    }
);

Then(
    /^I select the "([^"]*)" option from the "([^"]*)"$/,
    async function(this: ScenarioWorld, option: string, elementKey: ElementKey) {
        const {
            screen: { page },
            globalConfig,
        } = this;

        console.log(`I select the ${option} option from the ${elementKey}`);

        const elementIdentifier = getElementLocator(page, elementKey, globalConfig);

        await waitFor( async () => {
            //const elementStable = await waitForSelector(page, elementIdentifier);
            const result = await page.waitForSelector(elementIdentifier, {
                state: 'visible',
            });
            if(result) {
                await selectValue(page, elementIdentifier, option);
            }
            return result;
        });
    }
);